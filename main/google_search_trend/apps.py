from django.apps import AppConfig


class GoogleSearchTrendConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'google_search_trend'
